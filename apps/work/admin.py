from django.contrib import admin

from .models import Work, WorkImage

class WorkImageInline(admin.TabularInline):
    model = WorkImage
    extra = 4


class WorkAdmin(admin.ModelAdmin):
    inlines = [WorkImageInline]

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Work.objects.all()
        return Work.objects.filter(author=request.user)


admin.site.register(Work, WorkAdmin)
admin.site.register(WorkImage, admin.ModelAdmin)
