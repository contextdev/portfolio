from django.conf.urls import patterns, url

from .views import WorkListView

urlpatterns = patterns('',
                       url(r'^$', WorkListView.as_view(), name='work-list'),
                        )
