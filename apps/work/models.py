from django.db import models
from autoslug import AutoSlugField

from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver


class Work(models.Model):
    title = models.CharField(max_length=40)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    date = models.DateTimeField()
    slug = AutoSlugField(populate_from='title')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Work"
        verbose_name_plural = "Works"
        ordering = ["date"]


class WorkImage(models.Model):
    work = models.ForeignKey(Work, related_name='images')
    image = models.ImageField(upload_to='work', null=False, blank=False, )
    description = models.TextField(null=True, blank=True)

@receiver(pre_delete, sender=WorkImage)
def adImage_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.image.delete(False)

