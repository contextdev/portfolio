from django.core.mail import EmailMultiAlternatives
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

import json


@csrf_exempt
def contact(request):
    if request.method == 'POST':
        raw = request.read()
        data = json.loads(raw.decode("utf8"))

        contact = data.get('contact', '')
        email = data.get('email', '')
        phone = data.get('phone', '')
        description = data.get('description', '')

        html_content = "Datos de contactos : contacto : "+ contact + " email : " + email +  " phone : " + phone + " descripcion : " + description
        msg = EmailMultiAlternatives('Contact by page Context',
                                          html_content,
                                          'testnubiquo@gmail.com',
                                          [ 'contextinformatic@gmail.com'])
        msg.attach_alternative(html_content, 'text/html')
        msg.send()

        return HttpResponse("Mensaje guardado...")