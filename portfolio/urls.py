
from django.conf.urls import include, url
from django.contrib import admin
from .settings import MEDIA_ROOT
from .views import contact
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/marcos/matias/', include(admin.site.urls)),
    url(r'^contact/$', contact),
    url(r'^cv-marcos-barroso/$', TemplateView.as_view(template_name='cv_marcos.html'), name='cv-marcos'),
    url(r'^cv-matias-roson/$', TemplateView.as_view(template_name='cv_matias.html'), name='cv-matias'),
    url(r'^media/(?P<path>.*)$', "django.views.static.serve", {'document_root': MEDIA_ROOT}),
    url('', include("apps.work.urls", namespace="work")),

]
