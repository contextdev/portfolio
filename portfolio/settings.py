import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


SECRET_KEY = 'oo14%e+^x^iuxig_(sf)7bdpu6%#&i$p#l^c9w5uvm7(4(cw#^'

DEBUG = True

ALLOWED_HOSTS = []


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'sorl.thumbnail',
    'djmail',
    'apps.work'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.cache.CacheMiddleware', # cada página que no tenga parámetros GET o POST será puesta en cache por un cierto período de tiempo la primera vez que sean pedidas.
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'portfolio.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates"), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [

                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                #'material.frontend.context_processors.modules',
                ],
            },
        },
    ]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.static',
    "django.core.context_processors.request",

)

WSGI_APPLICATION = 'portfolio.wsgi.application'

# TEMPLATE_DIRS = (
#     os.path.join(BASE_DIR, "templates"),
# )

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
}


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"), )
STATIC_ROOT =  os.path.join(os.path.dirname(BASE_DIR), "static")

THUMBNAIL_DEBUG = True
THUMBNAIL_FORMAT = 'PNG'

SITE_ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
# Defined folder of media files to project
MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')

MEDIA_URL = "/media/"




# Config email

EMAIL_BACKEND="djmail.backends.async.EmailBackend"
DJMAIL_REAL_BACKEND="django.core.mail.backends.smtp.EmailBackend"

SITE_URL = "http://www.compraloahi.com.ar/"

DEFAULT_FROM_EMAIL = 'testnubiquo@gmail.com'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
EMAIL_HOST_PASSWORD = 'nubiquo1234567890'
EMAIL_USE_TLS = True


## CONFIGURATION FOR PRODUCTION

# Configured to send an e-mail to the site developers whenever your code raises an unhandled exception
ADMINS = (
    ('Marcos Barroso', 'mj1182@gmail.com'),
    ('Matias Roson', 'matiroson@gmail.com'),
)

# the option of receiving an e-mail any time somebody visits a page on your Django-powered site that
# raises 404 with a non-empty referrer – that is, every broken link.
MANAGERS = (
    ('Marcos Barroso', 'mj1182@gmail.com'),
    ('Matias Roson', 'matiroson@gmail.com'),
)



# CONFIG CACHE

# El tiempo en segundos que cada página será mantenida en la cache.
CACHE_MIDDLEWARE_SECONDS = 30000

# la cache middleware sólo colocará en cache pedidos anónimos
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True